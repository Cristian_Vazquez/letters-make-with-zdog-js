
const letraC = new Zdog.Illustration({
  element: '.zdog-c',
  dragRotate: true,
});

//Letra C
var c1 = new Zdog.Shape({
  addTo: letraC,
  path: [
    { x: 50, y: -50 },   // start
    { bezier: [
      { x:  -50, y: -70 }, // start control point
      { x:  -50, y:  70 }, // end control point
      { x:  50, y:  50 }, // end point
    ]},
  ],
  closed: false,
  stroke: 10,
  color: '#009E60'
});


function animate() {
  letraC.updateRenderGraph();
  requestAnimationFrame( animate );
}

animate();