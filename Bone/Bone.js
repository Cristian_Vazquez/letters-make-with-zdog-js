
//Se definen colores
var white = '#FFFFFF';

//Se define el espacio en donde se generara la vista de la figura
const Bone = new Zdog.Illustration({
  element: '.zdog-bone',
  dragRotate: true,
});


//Hueso
var Bone1 = new Zdog.Shape({
  addTo: Bone,
  path: [
    { x:  0, y:  40, z:  0 },
    { arc: [
      { x:  -10, y: -50,  Z: 0 },
      { x: 10, y:  -50,  Z: 0 }, // end point
    ]},
    { x:  0, y:  -40,  z:  0},
    
    { arc: [
      { x:  -10, y: 50,  Z: 0 }, // corner
      { x: 10, y:  50,  Z: 0 }, // end point
    ]}, 
  ],
  stroke: 20,
  color: white,
});


//Hueso
var Bone2 = new Zdog.Shape({
  addTo: Bone1,
  path: [
    { x:  0, y:  40, z:  0 },
    { arc: [
      { x:  10, y: -50,  Z: 0 }, // corner
      { x: -10, y:  -50,  Z: 0 }, // end point
    ]},
    
    { x:  0, y:  -40,  z:  0},  
    { arc: [
      { x:  10, y: 50,  Z: 0 }, // corner
      { x: -10, y:  50,  Z: 0 }, // end point
    ]},
  ],
  stroke: 20,
  color: white,
});




function animate() {
  Bone.updateRenderGraph();
  requestAnimationFrame( animate );
}

animate();