
//Se definen colores
var yellow = '#ED0';
var gold = '#EA0';
var orange = '#E62';
var garnet = '#C25';
const eggplant = '#636';

//Letra A
//Se define el espacio en donde se generara la vista de la figura
const letraA = new Zdog.Illustration({
  element: '.zdog-a',
  dragRotate: true,
});


//Letra A
var a1 = new Zdog.Shape({
  addTo: letraA,
  path: [
    { x: -50 }, // start at 1st point
    { move: { x: -60, y: 100 } },
    { x:  -10 }, // line to 2nd point
  ],
  stroke: 10,
  color: '#009E60',
});


var a2 = new Zdog.Shape({
  addTo: a1,
  path: [
    { x: -60}, // start at 1st point
    { move: { x: 35, y: 100 } },
    { x:  -10}, // line to 2nd point
  ],
  stroke: 10,
  color: '#009E60',
});


var a3 = new Zdog.Shape({
  addTo: a2,
  path: [
    { x: 0, y: 0}, // start at 1st point
    { move: { x: 15, y: 60 } },
    { x:  -40, y: 60}, // line to 2nd point
  ],
  stroke: 10,
  color: '#009E60',
});




function animate2() {
  letraA.updateRenderGraph();
  requestAnimationFrame( animate2 );
}

animate2();